package com.hls.linhas.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.hls.linhas.models.Shape;
import com.hls.linhas.repository.ShapeRepository;

@RestController
@RequestMapping("/shape")
public class ShapeController {
	
	//Autowired cria uma instancia automaticamente
	@Autowired
	private ShapeRepository repository;
	
	//Método GET Padrao
	@GetMapping
	public List<Shape> getListaLinhas() {
		return repository.findAll();
	}
	
	//Método POST Padrao
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	//Corpo da requisicao sera convertido para o obj Linha
	public Shape saveLinha(@RequestBody Shape linha) {
		try {
			return repository.save(linha);
		}catch(Exception ex) {
			System.out.println("Ops..." + ex.getMessage());
			return null;
		}
	}

}
