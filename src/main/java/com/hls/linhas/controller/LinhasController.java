package com.hls.linhas.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.hls.linhas.helpers.DatabaseLoader;
import com.hls.linhas.models.LinhaGTFS;
import com.hls.linhas.repository.LinhaRepository;

@RestController
@RequestMapping("/linhas")
public class LinhasController {
	
	//Autowired cria uma instancia automaticamente
	@Autowired
	private LinhaRepository linhaRepository;
	
	//Método GET Padrao
	@GetMapping
	public List<LinhaGTFS> getListaLinhas() {
		return linhaRepository.findAll();
	}
	
/*
	//Corpo da requisicao sera convertido para o obj Linha
	public LinhaGTFS saveLinha(@RequestBody LinhaGTFS linha) {
	}
	*/
	
	//Método POST Padrao
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public void loadLinhas() {
		
		if(getListaLinhas().size() == 0) {
			for(String line : DatabaseLoader.getFileContent("trips")) {
				line = line.replace("\"", "");
				System.out.println("Linha "+ line);
				String[] splitted = line.split(",");
				LinhaGTFS linha = new LinhaGTFS(splitted);
				linhaRepository.save(linha);
				
			}
		}
		
	}

}
