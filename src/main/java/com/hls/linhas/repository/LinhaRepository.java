package com.hls.linhas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hls.linhas.models.LinhaGTFS;

@Repository
public interface LinhaRepository extends JpaRepository<LinhaGTFS, Long> {}