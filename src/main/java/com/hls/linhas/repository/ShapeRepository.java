package com.hls.linhas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hls.linhas.models.Shape;

@Repository
public interface ShapeRepository extends JpaRepository<Shape, Long> {}

