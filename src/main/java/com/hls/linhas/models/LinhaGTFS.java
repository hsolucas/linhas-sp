package com.hls.linhas.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class LinhaGTFS {
	
	public LinhaGTFS() {}
	
	public LinhaGTFS(String[] csvLine) {
		try {
			this.setCodigo(csvLine[0]);
			this.setDiasOperacao(csvLine[1]);
			this.setLetreiro(csvLine[3]);
			this.setSentido(Integer.parseInt(csvLine[4]));
			this.setShapeId(Long.parseLong(csvLine[5]));
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long id;
	
	private String codigo;
	
	private String diasOperacao;
	
	private String letreiro;
	
	private Integer sentido;
	
	private Long shapeId;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDiasOperacao() {
		return diasOperacao;
	}

	public void setDiasOperacao(String diasOperacao) {
		this.diasOperacao = diasOperacao;
	}

	public String getLetreiro() {
		return letreiro;
	}

	public void setLetreiro(String letreiro) {
		this.letreiro = letreiro;
	}

	public Integer getSentido() {
		return sentido;
	}

	public void setSentido(Integer sentido) {
		this.sentido = sentido;
	}

	public Long getShapeId() {
		return shapeId;
	}

	public void setShapeId(Long shapeId) {
		this.shapeId = shapeId;
	}

	
}
