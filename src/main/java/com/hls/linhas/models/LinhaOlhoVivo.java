package com.hls.linhas.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class LinhaOlhoVivo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long id;
	
	private Long codigoSpTrans;
	
	private Boolean circular;
	
	@Column(nullable = false)
	private String numero;
	
	private Integer tipoAtendimento;
	
	@Column(nullable = false)
	private String letreiroIda;
	
	private String letreiroVolta;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
	public Long getCodigoSpTrans() {
		return codigoSpTrans;
	}

	public void setCodigoSpTrans(Long codigoSpTrans) {
		this.codigoSpTrans = codigoSpTrans;
	}

	public Boolean getCircular() {
		return circular;
	}

	public void setCircular(Boolean circular) {
		this.circular = circular;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Integer getTipoAtendimento() {
		return tipoAtendimento;
	}

	public void setTipoAtendimento(Integer tipoAtendimento) {
		this.tipoAtendimento = tipoAtendimento;
	}

	public String getLetreiroIda() {
		return letreiroIda;
	}

	public void setLetreiroIda(String letreiroIda) {
		this.letreiroIda = letreiroIda;
	}

	public String getLetreiroVolta() {
		return letreiroVolta;
	}

	public void setLetreiroVolta(String letreiroVolta) {
		this.letreiroVolta = letreiroVolta;
	}

	
}
