package com.hls.linhas.helpers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import org.springframework.util.ResourceUtils;

public class DatabaseLoader {

	public static List<String> getFileContent(String fileName) {
		try {
			//Base path: src/main/resources
			File file = ResourceUtils.getFile("classpath:data/" + fileName + ".txt");
			List<String> content = Files.readAllLines(file.toPath());
			return content;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
}
